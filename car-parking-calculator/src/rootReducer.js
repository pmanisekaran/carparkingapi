import { combineReducers } from "redux";
import parkingReducer from "./reducer";

export default combineReducers({
    parkingReducer
});