import React from "react";
import ReactDOM from "react-dom";
import ParkingDetails from "./Components/ParkingDetails";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";
import './index.css'
const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

function App() {
  return (
    <div className="App">
      <App />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <ParkingDetails />
  </Provider>,
  rootElement
);