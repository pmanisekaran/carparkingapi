function GetParkingFees(startDate,endDate) {
    return fetch(`http://localhost:49821/CarParking`, {
        method: 'POST',
        body: JSON.stringify({
            carEntryDateTime : startDate,
            carExitDateTime: endDate
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

export function FetchParkingDetails(startDate,endDate) {
    return dispatch => {
        dispatch(fetchParkingBegin());
        return GetParkingFees(startDate,endDate)
            .then(res => {                
                if (!res.ok) {
                    throw Error(res.statusText);
                }
                return res.json();
            })
            .then((data) => {                
                dispatch(fetchParkingSuccess(data));
                return data;
            })
            .catch(error =>
                dispatch(fetchParkingFailure(error))            
            );
    };
}

export const FETCH_PARKING_BEGIN = "FETCH_PARKING_BEGIN";
export const FETCH_PARKING_SUCCESS ="FETCH_PARKING_SUCCESS";
export const FETCH_PARKING_FAILURE ="FETCH_PARKING_FAILURE";

export const fetchParkingBegin = () => ({
    type: FETCH_PARKING_BEGIN
});

export const fetchParkingSuccess = parkingRate => ({
    type: FETCH_PARKING_SUCCESS,
    payload: {
        parkingRate
    }
});

export const fetchParkingFailure = error => ({
    type: FETCH_PARKING_FAILURE,
    payload: {
        error
    }
});