import React from "react";
import { connect } from "react-redux";
import { FetchParkingDetails } from "../Actions";
import moment from 'moment';
import { DatetimePickerTrigger } from 'rc-datetime-picker';
import "../../node_modules/rc-datetime-picker/dist/picker.css";

class ParkingDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      startDate: moment(),
      endDate: moment(),
      parkingRate: {}
    };
  }
  
  startDateChange = (moment) => {
    return this.setState({
      startDate:moment
    });
  }

  endDateChange = (moment) => {
    return this.setState({
     endDate:moment
    });
  }

  getCarParkingDetails(startDate,endDate) {
    this.props.dispatch(FetchParkingDetails(startDate.format('MM-DD-YYYY HH:mm').toString(),endDate.format('MM-DD-YYYY HH:mm').toString()));
  }

  render() {
    const { error, loading, details } = this.props;   
    if (loading) {
      return <div>Loading...</div>;
    }
    return (
    <div id="container">
        <div className="parking-form">
        <h3>Car Parking Details</h3>
        <table className="margin-bottom">
            <tbody>
            <tr>
            <td><label className="pull-left">Entry Date:&nbsp;&nbsp;</label></td>
            <td><DatetimePickerTrigger moment={this.state.startDate} onChange={this.startDateChange}>
                <input type="text" value={this.state.startDate.format('MM-DD-YYYY HH:mm')} readOnly />
                </DatetimePickerTrigger></td>
            </tr>
            <tr>
                <td><label className="pull-left">Exit Date:&nbsp;&nbsp;</label></td>
                <td><DatetimePickerTrigger moment={this.state.endDate} onChange={this.endDateChange}>
                <input type="text" value={this.state.endDate.format('MM-DD-YYYY HH:mm')} readOnly />
                </DatetimePickerTrigger></td>
                </tr>
            </tbody>
            </table>
            <button className="btn-submit btn" type="button" onClick={() =>this.getCarParkingDetails(this.state.startDate,this.state.endDate)}>Submit</button>
        </div>
        <div className="parking-details">        
        {error ? <div>{error.message}</div> : null}
        { details.nameOfTheRate ? <div>
                <h4>Car Parking Details</h4>
                {/* <button className="btn margin-bottom" type="button" onClick={() => window.print()}>Print</button> */}
                <div className="print-details">
                    <label>Name Of The Rate:&nbsp;&nbsp;</label><label>{details.nameOfTheRate}</label><br></br>
                    <label>Type:&nbsp;&nbsp;</label><label>{details.type}</label><br></br>
                    <label>Total Price:&nbsp;&nbsp;</label><label>{details.totalPrice}</label><br></br>
                    <label>Entry Condition:&nbsp;&nbsp;</label><label>{details.entryCondition}</label><br></br>
                    <label>Exit Condition:&nbsp;&nbsp;</label><label>{details.exitCondition}</label><br></br>
                </div></div> : null
            }
        </div>
     </div>
    )
  }
}

const mapStateToProps = state => ({
    details: state.parkingReducer.parkingRate,
    loading: state.parkingReducer.loading,
    error: state.parkingReducer.error
  });

export default connect(mapStateToProps)(ParkingDetails);