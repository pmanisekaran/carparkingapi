using car_parking_api.Implementation.Services;
using car_parking_api.Models.Response;
using car_parking_api.Requests.Models;
using NUnit.Framework;

namespace car_parking_api.nunit
{
    public class CarPakingTests
    {
        private CarParkingService _carParkingService = new CarParkingService();

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGetCarParkingFee()
        {
            //early bird
            CarParkingResponse carParkingResponse = _carParkingService.GetCarParkingRate(new CarParkingRequest() { 
                CarEntryDateTime = "12-12-2019 08:00",
                CarExitDateTime = "12-12-2019 16:00"
            });

            Assert.IsTrue(carParkingResponse.TotalPrice == "$13.00", "Rate should be $13.00");

            //night rate
            carParkingResponse = _carParkingService.GetCarParkingRate(new CarParkingRequest()
            {
                CarEntryDateTime = "12-12-2019 18:00",
                CarExitDateTime = "12-13-2019 16:30"
            });

            Assert.IsTrue(carParkingResponse.TotalPrice == "$6.50", "Rate should be $6.50");

            //weekend
            carParkingResponse = _carParkingService.GetCarParkingRate(new CarParkingRequest()
            {
                CarEntryDateTime = "12-14-2019 18:00",
                CarExitDateTime = "12-15-2019 16:30"
            });

            Assert.IsTrue(carParkingResponse.TotalPrice == "$10.00", "Rate should be $10.00");

            //hourly rate
            carParkingResponse = _carParkingService.GetCarParkingRate(new CarParkingRequest()
            {
                CarEntryDateTime = "12-13-2019 11:00",
                CarExitDateTime = "12-13-2019 13:30"
            });

            Assert.IsTrue(carParkingResponse.TotalPrice == "$15.00", "Rate should be $15.00");
        }
    }
}