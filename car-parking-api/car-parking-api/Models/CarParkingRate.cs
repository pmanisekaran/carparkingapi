﻿using System;
using static car_parking_api.Helpers.Enums;

namespace car_parking_api.Models
{
    public class CarParkingRate
    {
        public string NameOfTheRate { get; set; }
        public string Type { get; set; }
        public decimal TotalPrice { get; set; }
        public TimeSpan EntryMin { get; set; }
        public TimeSpan EntryMax { get; set; }
        public TimeSpan ExitMin { get; set; }
        public TimeSpan ExitMax { get; set; }
        public RateApplied RateApplied { get; set; }

        public string EntryCondition { get; set; }
        public string ExitCondition { get; set; }

        public int DayDifference { get; set; }
    }
}
