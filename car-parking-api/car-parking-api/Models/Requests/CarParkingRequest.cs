﻿namespace car_parking_api.Requests.Models
{
    public class CarParkingRequest
    {
        public string CarEntryDateTime { get; set; }

        public string CarExitDateTime { get; set; }
    }
}
