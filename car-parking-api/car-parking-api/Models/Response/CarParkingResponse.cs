﻿namespace car_parking_api.Models.Response
{
    public class CarParkingResponse
    {
        public string NameOfTheRate { get; set; }
        public string Type { get; set; }
        public string TotalPrice { get; set; }
        public string EntryCondition { get; set; }
        public string ExitCondition { get; set; }


    }
}
