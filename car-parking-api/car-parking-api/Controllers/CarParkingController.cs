﻿using car_parking_api.Interfaces.Services;
using car_parking_api.Models.Response;
using car_parking_api.Requests.Models;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using System;

namespace car_parking_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarParkingController : ControllerBase
    {
        private readonly ICarParkingService carParkingService;

        public CarParkingController(ICarParkingService carParkingService)
        {
            this.carParkingService = carParkingService;
        }

        [HttpPost]
        public object Get(CarParkingRequest carParkingRequest)
        {
            try
            {
                return carParkingService.GetCarParkingRate(carParkingRequest);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = ex.Message;
                return null;
            }
        }
    }
}
