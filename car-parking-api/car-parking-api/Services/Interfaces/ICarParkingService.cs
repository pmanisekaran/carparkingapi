﻿using car_parking_api.Models.Response;
using car_parking_api.Requests.Models;

namespace car_parking_api.Interfaces.Services
{
    public interface ICarParkingService
    {
         CarParkingResponse GetCarParkingRate(CarParkingRequest carParkingRequest);
    }
}
