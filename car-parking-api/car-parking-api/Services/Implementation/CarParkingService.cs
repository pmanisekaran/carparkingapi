﻿using car_parking_api.Interfaces.Services;
using car_parking_api.Models;
using car_parking_api.Models.Response;
using System;
using System.Collections.Generic;
using static car_parking_api.Helpers.Enums;
using System.Linq;
using car_parking_api.Requests.Models;
using System.Globalization;

namespace car_parking_api.Implementation.Services
{
    public class CarParkingService : ICarParkingService
    {
        #region Variables
        private List<CarParkingRate> _rules = new List<CarParkingRate>() {
            new CarParkingRate(){
                NameOfTheRate = "Early Bird",
                Type = "Flat Rate",
                TotalPrice = 13,
                EntryMin = new TimeSpan(6,0,0),
                EntryMax = new TimeSpan(9,0,0),
                ExitMin = new TimeSpan(15,30,0),
                ExitMax = new TimeSpan(23,30,0),
                RateApplied = RateApplied.Weekdays,
                EntryCondition = "Enter between 6:00 AM to 9:00 AM",
                ExitCondition = "Exit between 3:30 PM to 11:30 PM",
                DayDifference = 1
            },
            new CarParkingRate(){
                NameOfTheRate = "Night Rate",
                Type = "Flat Rate",
                TotalPrice = 6.5M,
                EntryMin = new TimeSpan(18,0,0),
                EntryMax = new TimeSpan(23,59,59),
                ExitMin = new TimeSpan(0,0,0),
                ExitMax = new TimeSpan(6,0,0),
                RateApplied = RateApplied.Weekdays,
                EntryCondition = "Enter between 6:00 PM to midnight (weekdays)",
                ExitCondition = "Exit between 3:30 PM to 11:30 PM",
                DayDifference = 1
            },
            new CarParkingRate(){
                NameOfTheRate = "Weekend Rate",
                Type = "Flat Rate",
                TotalPrice = 10,
                EntryMin = new TimeSpan(18,0,0),
                EntryMax = new TimeSpan(23,59,59),
                ExitMin = new TimeSpan(15,30,0),
                ExitMax = new TimeSpan(23,30,0),
                RateApplied = RateApplied.Weekends,
                EntryCondition = "Enter anytime past midnight on Friday to Sunday",
                ExitCondition = "Exit any time before midnight of Sunday"
            },
        };
        #endregion

        #region Methods
        private CarParkingResponse GetCarParkingResponse(CarParkingRate carParkingRate, DateTime entryDateTime, DateTime exitDateTime)
        {
            CarParkingResponse carParkingResponse = new CarParkingResponse();
            if (carParkingRate != null)
            {
                carParkingResponse.NameOfTheRate = carParkingRate.NameOfTheRate;
                carParkingResponse.Type = carParkingRate.Type;
                carParkingResponse.TotalPrice = string.Format("${0:0.00}", carParkingRate.TotalPrice);
                carParkingResponse.EntryCondition = carParkingRate.EntryCondition;
                carParkingResponse.ExitCondition = carParkingRate.ExitCondition;
            }
            else
            {
                TimeSpan difference = exitDateTime - entryDateTime;

                carParkingResponse.NameOfTheRate = "Standard Rate";
                carParkingResponse.Type = "Hourly Rate";

                int price = 0;

                if (difference.TotalMinutes > 180)
                    price = (int)Math.Ceiling(difference.TotalDays) * 20;
                else if (difference.TotalMinutes >= 120)
                    price = 15;
                else if (difference.TotalMinutes >= 60)
                    price = 10;
                else
                    price = 5;

                carParkingResponse.TotalPrice = string.Format("${0:0.00}", price);
            }

            return carParkingResponse;
        }

        public CarParkingResponse GetCarParkingRate(CarParkingRequest carParkingRequest)
        {

            if (string.IsNullOrEmpty(carParkingRequest.CarExitDateTime) || string.IsNullOrEmpty(carParkingRequest.CarEntryDateTime))
                throw new Exception("Exit or Entry time was null");

            //datetime is taken as string in the class because on different machines / servers this can potentially be a problem due to date formats so we fix format for frontend and backend
            DateTime entryDateTime = DateTime.ParseExact(carParkingRequest.CarEntryDateTime, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture);
            DateTime exitDateTime = DateTime.ParseExact(carParkingRequest.CarExitDateTime, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture);

            if (entryDateTime > exitDateTime)
                throw new Exception("Exit time should be greater than entry time");

            CarParkingResponse carParkingResponse = null;

            //first check if the entry and exit both fall in midnight?
            if ((entryDateTime.DayOfWeek == DayOfWeek.Saturday ||
                entryDateTime.DayOfWeek == DayOfWeek.Sunday) &&
                (exitDateTime.DayOfWeek == DayOfWeek.Saturday ||
                exitDateTime.DayOfWeek == DayOfWeek.Sunday)
              )
            {
                CarParkingRate carParkingRate = _rules.First(r => r.RateApplied == RateApplied.Weekends);
                carParkingResponse = GetCarParkingResponse(carParkingRate, entryDateTime, exitDateTime);
            }
            else
            {
                CarParkingRate carParkingRate = _rules.SingleOrDefault(r => entryDateTime.TimeOfDay >= r.EntryMin && entryDateTime.TimeOfDay <= r.EntryMax
                                && exitDateTime.TimeOfDay >= r.ExitMin && exitDateTime.TimeOfDay <= r.ExitMax && r.RateApplied == RateApplied.Weekdays
                                && (exitDateTime - entryDateTime).TotalMinutes <= (r.DayDifference * 24 * 60));

                carParkingResponse = GetCarParkingResponse(carParkingRate, entryDateTime, exitDateTime);

            }

            return carParkingResponse;

        }
        #endregion
    }
}
